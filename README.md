# bulk_file_rename

This projecct was designed to rename serial episodes file names in the format ```name S01E01.mp4```
The program will find the extension automaticaly


## Getting Started
copy the file ```run.py``` to the directory with the files to be renamed 
run the file
```python3 run.py```

enter the series name and season


if you want to give a specific directory modify the ```path``` varible in the code to the specific value
example given in code

### Prerequisites

The software requires python3


## Authors

* **Paul Antony**


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


