# class used in file renamer process

import os



class file_rename:

	@staticmethod
	def rename(path, old_name, new_name):

		for i in range(len(old_name)):
			os.rename(os.path.join(path, old_name[i]), os.path.join(path, new_name[i]))
			print("{}  =>  {}".format(old_name[i], new_name[i]))
	



	@staticmethod
	def season_renamer(path, series_name, season):
		
		old_name = os.listdir(path)
		old_name.sort()
		episode = 0
		new_name = []

		for file in old_name:

			#extension finder
			file_type = ""
			for i in file[::-1]:
				if i == '.':
					break
				file_type = i+file_type
			file_type = '.' + file_type
			episode = episode +1
			new_name.append("{} S{:02d}E{:02d}{}".format(series_name,season,episode,file_type))
		
		file_rename.rename(path, old_name, new_name)



	@staticmethod
	def series_renamer(path, series_name):
		new_name = os.path.join(os.path.dirname(path) ,series_name)
		os.rename(path,new_name)
		path = new_name

		folder = os.listdir(path)
		folder.sort()

		season = 1
		for old_season in folder:
			new_season = "{} Season {:02d}".format(series_name, season)
			os.rename(os.path.join(path, old_season), os.path.join(path, new_season))
			print("__________{} --> {}__________".format(old_season, new_season))
			file_rename.season_renamer(os.path.join(path, new_season),series_name,season)
			season += 1
		new_name = os.path.join(os.path.dirname(path) ,series_name)




	@staticmethod
	def dummey_data(type = 1,path = os.getcwd()):
		if type == 1:

			path = os.path.join(path, "dummey")

			os.mkdir(path)
			for i in range(1,11):
				open(os.path.join(path, "dummey {}.txt".format(i)), 'a').close()

		elif type == 2:
			path = os.getcwd()
			path = os.path.join(path, "dummey")

			os.mkdir(path)
			for folder in range(1,6):

				f_path = os.path.join(path, "Dummey Season " + str(folder))
				os.mkdir(f_path)
				for i in range(1,11):
					open(os.path.join(f_path, "dummey s{}e{}.txt".format(folder,i)), 'a').close()


if __name__ == "__main__":
	#file_rename.dummey_data(2)
	#file_rename.series_renamer("/home/paul/Projects/bulk_file_rename/test","test")